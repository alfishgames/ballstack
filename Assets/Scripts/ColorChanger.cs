﻿using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    public RollingBall[] rollingBallBlue, rollingBallGreen, rollingBallOrange;
    public List<GameObject> touchedBlue = new List<GameObject>();
    public List<GameObject> touchedGreen = new List<GameObject>();
    public List<GameObject> touchedOrange = new List<GameObject>();
    public MeshRenderer myMeshRenderer;
    public Collider myCol;
    public Material matBlue, matGreen, matOrange;
    public Material playerBlue, playerGreen, playerOrange;
    public bool CCBlue, CCGreen, CCOrange;

    private void Awake()
    {
        myMeshRenderer = GetComponent<MeshRenderer>();
        myCol = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {


        if (CCGreen)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                CatchBall.instance.playerOrange = false;
                CatchBall.instance.playerBlue = false;
                CatchBall.instance.playerGreen = true;
                CatchBall.instance.myRenderer.material = playerGreen;
            }

            if (other.gameObject.CompareTag("Ball_Blue"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedBlue.Add(other.gameObject);

                rollingBallBlue = new RollingBall[touchedBlue.Count];
                for (int i = 0; i < touchedBlue.Count; i++)
                {
                    rollingBallBlue[i] = touchedBlue[i].GetComponent<RollingBall>();
                    touchedBlue[i].gameObject.tag = "Ball_Green";
                    rollingBallBlue[i].amIBlue = false;
                    rollingBallBlue[i].amIOrange = false;
                    rollingBallBlue[i].amIGreen = true; 
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }

            if (other.gameObject.CompareTag("Ball_Orange"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedOrange.Add(other.gameObject);

                rollingBallOrange = new RollingBall[touchedOrange.Count];
                for (int i = 0; i < touchedOrange.Count; i++)
                {
                    rollingBallOrange[i] = touchedOrange[i].GetComponent<RollingBall>();
                    touchedOrange[i].gameObject.tag = "Ball_Green";
                    rollingBallOrange[i].amIOrange = false;
                    rollingBallOrange[i].amIBlue = false;
                    rollingBallOrange[i].amIGreen = true;
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }
        }

        if (CCBlue)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                CatchBall.instance.playerGreen = false;
                CatchBall.instance.playerOrange = false;
                CatchBall.instance.playerBlue = true;
                CatchBall.instance.myRenderer.material = playerBlue;
            }

            if (other.gameObject.CompareTag("Ball_Green"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedGreen.Add(other.gameObject);

                rollingBallGreen = new RollingBall[touchedGreen.Count];
                for (int i = 0; i < touchedGreen.Count; i++)
                {
                    rollingBallGreen[i] = touchedGreen[i].GetComponent<RollingBall>();
                    touchedGreen[i].gameObject.tag = "Ball_Blue";
                    rollingBallGreen[i].amIGreen = false;
                    rollingBallGreen[i].amIOrange = false;
                    rollingBallGreen[i].amIBlue = true;
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }

            if (other.gameObject.CompareTag("Ball_Orange"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedOrange.Add(other.gameObject);

                rollingBallOrange = new RollingBall[touchedOrange.Count];
                for (int i = 0; i < touchedOrange.Count; i++)
                {
                    rollingBallOrange[i] = touchedOrange[i].GetComponent<RollingBall>();
                    touchedOrange[i].gameObject.tag = "Ball_Blue";
                    rollingBallOrange[i].amIOrange = false;
                    rollingBallOrange[i].amIGreen = false;
                    rollingBallOrange[i].amIBlue = true;
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }
        }

        if (CCOrange)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                CatchBall.instance.playerBlue = false;
                CatchBall.instance.playerGreen = false;
                CatchBall.instance.playerOrange = true;
                CatchBall.instance.myRenderer.material = playerOrange;
            }

            if (other.gameObject.CompareTag("Ball_Green"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedGreen.Add(other.gameObject);

                rollingBallGreen = new RollingBall[touchedGreen.Count];
                for (int i = 0; i < touchedGreen.Count; i++)
                {
                    rollingBallGreen[i] = touchedGreen[i].GetComponent<RollingBall>();
                    touchedGreen[i].gameObject.tag = "Ball_Orange";
                    rollingBallGreen[i].amIGreen = false;
                    rollingBallGreen[i].amIOrange = true;
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }

            if (other.gameObject.CompareTag("Ball_Blue"))
            {
                GameObject go = other.gameObject;
                Physics.IgnoreCollision(go.GetComponent<RollingBall>().myTriggerCol, myCol);
                touchedBlue.Add(other.gameObject);

                rollingBallBlue = new RollingBall[touchedBlue.Count];
                for (int i = 0; i < touchedBlue.Count; i++)
                {
                    rollingBallBlue[i] = touchedBlue[i].GetComponent<RollingBall>();
                    touchedBlue[i].gameObject.tag = "Ball_Orange";
                    rollingBallBlue[i].amIBlue = false;
                    rollingBallBlue[i].amIGreen = false;
                    rollingBallBlue[i].amIOrange = true;
                }

                touchedBlue.Clear();
                touchedGreen.Clear();
                touchedOrange.Clear();
            }
        }
    }
}
