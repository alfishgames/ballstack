﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour
{
    public GameObject startPanel;

    public void FirstStart()
    {
        startPanel.SetActive(false);

        RollingBall[] rollingBalls = FindObjectsOfType<RollingBall>();
        for (int i = 0; i < rollingBalls.Length; i++)
        {
            rollingBalls[i].gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
