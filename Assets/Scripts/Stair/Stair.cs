﻿using System.Collections.Generic;
using UnityEngine;

public class Stair : MonoBehaviour
{
    public List<GameObject> ballHolder = new List<GameObject>();
    public Collider myTriggerCol, playerCol;
    RollingBall[] rollingBall;
    Player player;
    public StairController stairController;

    private void Awake()
    {
        stairController = GameObject.Find("Stair_Final").GetComponent<StairController>();
        playerCol = GameObject.FindGameObjectWithTag("Player").GetComponent<CapsuleCollider>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    private void Update()
    {
        Physics.IgnoreCollision(myTriggerCol, playerCol);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player_Finish") && transform.CompareTag("Stair_Finish"))
        {
            player.stopForce = 0f;
            player.isMoveable = false;
            MenuManager.instance.SuccessFinishPanel();
            

        }

        if (other.gameObject.CompareTag("Ball_Blue") || other.gameObject.CompareTag("Ball_Green") || other.gameObject.CompareTag("Ball_Orange"))
        {
            GameObject ball = other.gameObject;
            ballHolder.Add(ball);
            rollingBall = new RollingBall[ballHolder.Count];
            for (int i = 0; i < ballHolder.Count; i++)
            {
                rollingBall[i] = ballHolder[i].GetComponent<RollingBall>();
                rollingBall[i].followPlayer = false;
                rollingBall[i].myRb.velocity = Vector3.zero;
            }
        }
    }
}
