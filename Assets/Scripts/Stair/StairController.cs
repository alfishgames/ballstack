﻿using System.Collections.Generic;
using UnityEngine;

public class StairController : MonoBehaviour
{
    public List<GameObject> balls = new List<GameObject>();
    public List<GameObject> myStairs = new List<GameObject>();
    public Vector3 offset;
    public bool obstacle, setTimer;
    public float lerpRatio;
    public float timer;

    private float lerpValue;
    private float stairPassTime = .170f;
    private int playerStairNumber;
    private bool stairFail;

    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            myStairs.Add(gameObject.transform.GetChild(i).gameObject);
        }
    }

    private void Update()
    {
        PlayerPlacer();
    }

    private void PlayerPlacer()
    {
        playerStairNumber = CatchBall.instance.myList.Count;


        if (obstacle)
        {
            if (setTimer)
            {
                timer += Time.deltaTime;
            }

            if (timer >= stairPassTime * transform.childCount && setTimer)
            {
                if (myStairs.Count <= CatchBall.instance.myList.Count)
                {
                    for (int i = 0; i < myStairs.Count; i++)
                    {
                        CatchBall.instance.myList.RemoveAt(CatchBall.instance.myList.Count-1);
                        setTimer = false;
                    }
                }
            }
            if (setTimer)
            {
                if (myStairs.Count > CatchBall.instance.myList.Count)
                {
                    if (!stairFail)
                    {
                        if (timer >= stairPassTime * CatchBall.instance.myList.Count)
                        {
                            Player.instance.isMoveable = false;
                            Player.instance.setColliderState(true);
                            Player.instance.setRigidbodyState(false);
                            Player.instance.playerAnimation.enabled = false;
                            MenuManager.instance.FailedFinishPanel();
                            stairFail = true;
                        }
                    }
                }
            }
        }

        if (!obstacle)
        {
            if (setTimer)
            {
                if (!Player.instance.isMoveable)
                {
                    lerpValue += Time.deltaTime / lerpRatio;
                    Player.instance.transform.position = Vector3.Lerp(Player.instance.transform.position, myStairs[playerStairNumber].transform.position + offset, lerpValue);
                    //GameHandler.instance.gameSuccess = true;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball_Blue") || other.gameObject.CompareTag("Ball_Blue") || other.gameObject.CompareTag("Ball_Orange"))
        {
            GameObject go = other.gameObject;
            balls.Add(go);
        }

        if (other.gameObject.CompareTag("Player"))
        {
            setTimer = true;
        }
    }
}
