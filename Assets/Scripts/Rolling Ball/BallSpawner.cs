﻿using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject spawnBallsGameObject;
    public GameObject[] declaredBall;
    public RollingBall[] rollB;

    private void Start()
    {
        declaredBall = new GameObject[spawnBallsGameObject.transform.childCount];
        rollB = new RollingBall[declaredBall.Length];

        for (int i = 0; i < declaredBall.Length; i++)
        {
            declaredBall[i] = spawnBallsGameObject.transform.GetChild(i).gameObject;
            rollB[i] = declaredBall[i].GetComponent<RollingBall>();
            rollB[i].rollingSpeed = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            for (int i = 0; i < declaredBall.Length; i++)
            {
                rollB[i].rollingSpeed = 300f;
            }
        }

    }
}
