﻿using UnityEngine;

public class RollingBall : MonoBehaviour
{
    CatchBall cB;

    public Material ballBlue, ballGreen, ballOrange;
    public Collider myTriggerCol, playerCol;
    public Transform player;
    public GameObject childBasket, childPool, childTennis;
    public int myNumber, abc;
    public float timer, lerpRatio, k;
    public bool amIBlue, amIOrange, amIGreen, followPlayer;
    public Rigidbody myRb;
    [HideInInspector]
    public float rollingSpeed = 300f;

    private MeshRenderer ballRenderer;
    private Collider myCol;
    private float lerpValue;
    private Transform followTargetBall;

    Vector3 crashForce = new Vector3(2f, 1f, -1f);
    Vector3 myCrashForce = new Vector3(-2f, 1f, -1f);


    private void Awake()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Transform>();
        ballRenderer = GetComponent<MeshRenderer>();
        playerCol = GameObject.FindWithTag("Player").GetComponent<Collider>();
        myRb = GetComponent<Rigidbody>();
        myCol = GetComponent<Collider>();
        cB = GameObject.FindWithTag("Player").GetComponent<CatchBall>();

        myRb.isKinematic = true;
    }


    private void FixedUpdate()
    {
        if (followPlayer)
        {
            FollowPlayer();
            rollingSpeed = 0f;
        }
        else
        {
            MovementController();
        }
    }

    private void Update()
    {
        Physics.IgnoreCollision(myTriggerCol, playerCol);
    }

    void MovementController()
    {
        myRb.AddForce(Vector3.back * rollingSpeed * Time.deltaTime);
    }

    void FollowPlayer()
    {
        myRb.velocity = Vector3.zero;

        if (abc == 1)
        {
            lerpValue += Time.deltaTime / lerpRatio;
            Vector3 targetPos = new Vector3(followTargetBall.position.x, player.position.y, player.position.z);
            transform.position = Vector3.Lerp(transform.position, targetPos + Vector3.down * transform.localScale.z * .5f * abc, lerpValue);
        }
        else
        {
            lerpValue += Time.deltaTime / lerpRatio;
            Vector3 targetPos = new Vector3(followTargetBall.position.x, player.position.y, player.position.z);
            k = .78f;
            if (abc >= 3) k = .85f + .01f * abc;
            transform.position = Vector3.Lerp(transform.position, targetPos + Vector3.down * transform.localScale.z * 1f * abc * k, lerpValue);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (amIBlue)
        {
            childBasket.SetActive(false);
            childTennis.SetActive(false);
            childPool.SetActive(true);

            if (CatchBall.instance.playerBlue)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    Debug.Log("Mavi top eklendi!");
                    if (cB.myList.Count == 0)
                    {
                        followTargetBall = player.transform;
                    }
                    else
                    {
                        followTargetBall = cB.myList[cB.myList.Count - 1].transform;
                    }
                    cB.myList.Add(this.gameObject);
                    Physics.IgnoreCollision(myCol, playerCol);
                    abc = cB.myList.Count;
                    followPlayer = true;
                }
            }

            if (other.gameObject.CompareTag("Ball_Orange"))
            {
                Debug.Log("Turuncu topa degdi");
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 150f);
            }

            if (other.gameObject.CompareTag("Ball_Green"))
            {
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 15f);
            }
        }

        if (amIGreen)
        {
            childBasket.SetActive(false);
            childPool.SetActive(false);
            childTennis.SetActive(true);

            if (CatchBall.instance.playerGreen)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    Debug.Log("Yesil top eklendi!");
                    if (cB.myList.Count == 0)
                    {
                        followTargetBall = player.transform;
                    }
                    else
                    {
                        followTargetBall = cB.myList[cB.myList.Count - 1].transform;
                        Debug.Log(cB.myList[cB.myList.Count - 1].gameObject.name);
                    }
                    cB.myList.Add(this.gameObject);
                    Physics.IgnoreCollision(myCol, playerCol);
                    abc = cB.myList.Count;
                    followPlayer = true;
                }
            }

            if (other.gameObject.CompareTag("Ball_Orange"))
            {
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 150f);
            }

            if (other.gameObject.CompareTag("Ball_Blue"))
            {
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 15f);
            }
        }

        if (amIOrange)
        {
            childPool.SetActive(false);
            childTennis.SetActive(false);
            childBasket.SetActive(true);

            if (CatchBall.instance.playerOrange)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    Debug.Log("Turuncu top eklendi!");
                    if (cB.myList.Count == 0)
                    {
                        followTargetBall = player.transform;
                    }
                    else
                    {
                        followTargetBall = cB.myList[cB.myList.Count - 1].transform;
                    }
                    cB.myList.Add(this.gameObject);
                    Physics.IgnoreCollision(myCol, playerCol);
                    abc = cB.myList.Count;
                    followPlayer = true;
                }
            }

            if (other.gameObject.CompareTag("Ball_Green"))
            {
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 150f);
            }

            if (other.gameObject.CompareTag("Ball_Blue"))
            {
                if (followPlayer)
                {
                    followPlayer = false;
                    cB.myList.RemoveAt(cB.myList.Count - 1);
                }
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 15f);
            }
        }

    }
}
