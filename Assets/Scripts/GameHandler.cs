﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public static GameHandler instance;

    //UI Segment
    public GameObject inGameUI, mainMenuUI, pauseMenuUI, levelSectionUI, endGameMenuUI, tapToStartMenuUI;
    public Button nextLevelButton, retryButton, backButton;
    public Text gameState, scoreText, levelName;
    bool isGameOver = true;

    //[HideInInspector]
    public bool gameSuccess, gameOver;
    private bool firstTimePlay;
    private int sceneIndex, score;

    private void Awake()
    {
        #region Null Check
        if (instance == null)
        {
            instance = this;
        }

        else if (instance != null)
        {
            Destroy(this);
        }

        #endregion

   
    }

    private void Update()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        GameState();
       // UIManagement();
    }

    private void GameState()
    {
        if (sceneIndex == 0)
        {
         
        }

        if (gameOver)
        {
       
            CloseAllUIMenus();
            endGameMenuUI.SetActive(true);


            if (isGameOver) {  
            Player.instance.setColliderState(true);
            Player.instance.setRigidbodyState(false);
            Player.instance.playerAnimation.enabled = false;
            isGameOver = false;
            }

        }
        if (gameSuccess)
        {
      
            CloseAllUIMenus();
            endGameMenuUI.SetActive(true);
        }
    }

    public void UIManagement()
    {
        if (sceneIndex != 0)
        {
            score = CatchBall.instance.myList.Count;

            levelName.text = "Level " + (sceneIndex + 1).ToString();
            scoreText.text = "Score : " + score.ToString();

            if (gameOver)
            {
                //Texts
                gameState.text = "Oh no, You Lose!";
                //scoreText.text

                //Buttons
                nextLevelButton.enabled = false;
                retryButton.enabled = true;
            }

            if (gameSuccess)
            {
                //Texts
                gameState.text = "You Win!";

                //Buttons
                nextLevelButton.enabled = true;
                retryButton.enabled = false;
            }
        }
    }
    public void PlayButtonMainMenu()
    {
        mainMenuUI.SetActive(false);
       

        if (firstTimePlay)
        {
            SceneManager.LoadScene("First_Level");
        }
        else
        {
            levelSectionUI.SetActive(true);
        }
    }

    public void TapToStart()
    {
    
        CloseAllUIMenus();
        inGameUI.SetActive(true);
    }

    public void PauseButton()
    {
      
        CloseAllUIMenus();
        pauseMenuUI.SetActive(true);
    }

    public void ResumeGameButton()
    {
        
        CloseAllUIMenus();
        inGameUI.SetActive(true);
    }

    public void RetryLevelButton()
    {
      
        SceneManager.LoadScene(sceneIndex);
        endGameMenuUI.SetActive(false);
        mainMenuUI.SetActive(false);
        gameOver = false;
        gameSuccess = false;
    }

    public void NextLevelButton()
    {
       
        SceneManager.LoadScene(sceneIndex + 1, LoadSceneMode.Single);
    }

    public void BackToMainMenu()
    {
        CloseAllUIMenus();
        mainMenuUI.SetActive(true);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void LevelSection()
    {
     
        CloseAllUIMenus();
        levelSectionUI.SetActive(true);
    }

    private void CloseAllUIMenus()
    {
        inGameUI.SetActive(false);
        mainMenuUI.SetActive(false);
        pauseMenuUI.SetActive(false);
        endGameMenuUI.SetActive(false);
        levelSectionUI.SetActive(false);
        tapToStartMenuUI.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Game is shutting down.");
    }

}
