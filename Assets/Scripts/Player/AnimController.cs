﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        Debug.Log("Is it working?");
        if (CatchBall.instance.animJump)
        {
            CatchBall.instance.animJump = false;
            anim.SetTrigger("Jump1");
        }

        if (CatchBall.instance.animFall)
        {
            CatchBall.instance.animFall = false;
            anim.SetBool("Fall", true);
        }

        
    }
}
