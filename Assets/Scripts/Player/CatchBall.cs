﻿using System.Collections.Generic;
using UnityEngine;

public class CatchBall : MonoBehaviour
{
    public static CatchBall instance;
    public SkinnedMeshRenderer myRenderer;

    public bool playerBlue, playerOrange, playerGreen, animJump, animFall;
    public int ballCount;

    public List<GameObject> myList = new List<GameObject>();
    public int number;

    private CapsuleCollider m_Collider;
    private MenuManager menuManager;

    private void Awake()
    {
        #region instance
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(this);
        }
        #endregion
        menuManager = FindObjectOfType<MenuManager>();
    }

    private void Start()
    {
        playerBlue = true;
        m_Collider = GetComponent<CapsuleCollider>();
    }

    private void Update()
    {
        SetCollider();
        for (int i = 0; i < myList.Count; i++)
        {
                //myList[i].gameObject.GetComponent<RollingBall>().followPlayer = true;
        }
    }

    void SetCollider()
    {
        m_Collider.height = 1.4f * myList.Count + 1f;
    }

    private void OnTriggerEnter(Collider collider)
    {

        if (playerBlue)
        {
            if (collider.gameObject.CompareTag("Ball_Blue"))
            {
                playerBlue = true;
                collider.GetComponent<Animator>().enabled = true;
            }

            if (collider.gameObject.CompareTag("Ball_Orange") || collider.gameObject.CompareTag("Ball_Green"))
            {
                if (myList.Count == 0)
                {
                    Player.instance.isMoveable = false;
                    Player.instance.setColliderState(true);
                    Player.instance.setRigidbodyState(false);
                    Player.instance.playerAnimation.enabled = false;
                    menuManager.FailedFinishPanel();
                }
            }
        }

        if (playerOrange)
        {
            if (collider.gameObject.CompareTag("Ball_Orange"))
            {
                playerOrange = true;
                collider.GetComponent<Animator>().enabled = true;
                animJump = true;
            }

            if (collider.gameObject.CompareTag("Ball_Blue") || collider.gameObject.CompareTag("Ball_Green"))
            {
                if (myList.Count == 0)
                {
                    Player.instance.isMoveable = false;
                    Player.instance.setColliderState(true);
                    Player.instance.setRigidbodyState(false);
                    Player.instance.playerAnimation.enabled = false;
                    menuManager.FailedFinishPanel();

                }
            }
        }

        if (playerGreen)
        {
            if (collider.gameObject.CompareTag("Ball_Green"))
            {
                playerGreen = true;
                collider.GetComponent<Animator>().enabled = true;
                animJump = true;
            }
            if (collider.gameObject.CompareTag("Ball_Orange") || collider.gameObject.CompareTag("Ball_Blue"))
            {
                if (myList.Count == 0)
                {
                    Player.instance.isMoveable = false;
                    Player.instance.setColliderState(true);
                    Player.instance.setRigidbodyState(false);
                    Player.instance.playerAnimation.enabled = false;
                    menuManager.FailedFinishPanel();

                }
            }
        }
    }
}
