using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private CatchBall catchBall;
    private MyGameManager myGameManager;

    public static Player instance;
    public float horizontalSpeed;
    public float forwardSpeed;
    public float lerpRatio;
    public float stopForce;
    public bool isMoveable;

    private float lerpValue;
    private Vector3 moveVector;
    private Vector3 playerOffset;
    private Rigidbody rb;
    private bool firstStart, _animWin;

    public Animator playerAnimation;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        catchBall = GameObject.FindWithTag("Player").GetComponent<CatchBall>();
        stopForce = 1f;
        myGameManager = FindObjectOfType<MyGameManager>();
        setColliderState(false);
        setRigidbodyState(true);
    }

    private void Update()
    {
        if (MenuManager.instance.animWin && !_animWin)
        {
            _animWin = true;
            Debug.Log("Do win dance!");
            playerAnimation.SetBool("Run", false);
            playerAnimation.SetBool("Win", true);
        }
    }

    #region LifeTime
    void FixedUpdate()
    {
        if (!firstStart)
        {
            if (Input.GetMouseButton(0))
            {
                Debug.Log("How many times running this?");
                isMoveable = true;
                firstStart = true;
                playerAnimation.SetBool("Run", true);
                myGameManager.FirstStart();
            }
        }


        if (isMoveable)
        {
            lerpValue += Time.deltaTime / lerpRatio;
            playerOffset = Vector3.Lerp(playerOffset, Vector3.up * Mathf.Lerp(0f, catchBall.myList.Count * .5f, lerpValue), 8 * Time.deltaTime);

            moveVector.z = transform.position.z + (forwardSpeed * Time.fixedDeltaTime * stopForce);
            if (Input.GetMouseButton(0))
            {
                float mouseX = Input.GetAxis("Mouse X");
                if (mouseX > 3)
                {
                    mouseX = 3;
                }
                if (mouseX < -3)
                {
                    mouseX = -3;
                }
                Vector3 myPos = transform.localEulerAngles;
                Vector3 myTransform = transform.position;
                if (myTransform.x > 1.8f)
                {
                    myTransform.x = 1.8f;
                    transform.position = new Vector3(myTransform.x, transform.position.y, transform.position.z);
                }
                else if (myTransform.x < -1.8f)
                {
                    myTransform.x = -1.8f;
                    transform.position = new Vector3(myTransform.x, transform.position.y, transform.position.z);
                }

                moveVector.x = transform.position.x + mouseX * horizontalSpeed * Time.fixedDeltaTime;

                Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, mouseX * 30, 0));
                transform.rotation = Quaternion.Slerp(transform.rotation, deltaRotation, Time.deltaTime * 10);

            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, new Quaternion(transform.rotation.x, 0, transform.rotation.z, transform.rotation.w), Time.deltaTime * horizontalSpeed);
            }
            rb.MovePosition(Vector3.Lerp(moveVector, moveVector + playerOffset, lerpValue));
            //rb.MovePosition(Vector3.Lerp(moveVector + playerOffset, moveVector + playerOffset, lerpValue));
        }

    }
    #endregion


    #region Ragdoll
    public void setRigidbodyState(bool state)
    {

        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rigidbody in rigidbodies)
        {   

            rigidbody.isKinematic = state;

                  rigidbody.AddExplosionForce(UnityEngine.Random.Range(5, 500), transform.position, UnityEngine.Random.Range(1, 360), UnityEngine.Random.Range(1, 360),ForceMode.Force);


        }

        GetComponent<Rigidbody>().isKinematic = !state;

    }


   public void setColliderState(bool state)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = state;

            if (collider.gameObject.name == "Finish Triger")
            {
                collider.enabled = !state;
            }
           
        }

        try
        {
            GetComponent<Collider>().enabled = !state;
        }
        catch (Exception ex) { }

    }
    #endregion

}
