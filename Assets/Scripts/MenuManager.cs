﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    public GameObject successPanel;
    public GameObject failedPanel;
    public GameObject nextLevelButton;
    public GameObject restartButton;
    public Text levelText;

    //[HideInInspector]
    public bool animWin;

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        int levelNumber = PlayerPrefs.GetInt("CurrentLevel");
        if (levelNumber == 0)
        {
            levelText.text = "Level  " + (PlayerPrefs.GetInt("CurrentLevel") + 1).ToString();
        }
        else
        {
            levelText.text = "Level  " + (PlayerPrefs.GetInt("CurrentLevel")).ToString();
        }

        Application.targetFrameRate = 60;
        Invoke("SendDataGameAnalytic", 2f);

    }

    public void RestartButton()
    {
        PlayerPrefs.SetInt("IsRestart", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void NextLevelButton()
    {
        PlayerPrefs.SetInt("IsRestart", 0);
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel", 1) + 1);
        successPanel.gameObject.SetActive(false);
        LoadAsync.GlobalAsync.TriggerManually();
        TinySauce.OnGameFinished(PlayerPrefs.GetInt("CurrentLevel", 1).ToString(), true, 1);
    }


    public void SuccessFinishPanel()
    {
        successPanel.gameObject.SetActive(true);
        animWin = true;
    }

    public void FailedFinishPanel()
    {
        failedPanel.gameObject.SetActive(true);
    }

    private void SendDataGameAnalytic()
    {
        GameAnalytics.Initialize();
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Application.version, PlayerPrefs.GetInt("CurrentLevel", 1).ToString());

        TinySauce.OnGameStarted(PlayerPrefs.GetInt("CurrentLevel", 1).ToString());


    }

}



