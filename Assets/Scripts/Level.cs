﻿using UnityEngine;

public class Level : MonoBehaviour
{
    public bool bonusLevel;

    [HideInInspector]
    public string levelName;

    private void Start()
    {
        if (bonusLevel)
        {
            levelName = "Bonus Level";
            Player.instance.forwardSpeed = 8.5f;
        }
        else
        {
            levelName = transform.name;
            Player.instance.forwardSpeed = 5.5f;
        }
    }

}
